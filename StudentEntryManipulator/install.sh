DEST=/home/sotiris/Projects/ProjectsCppTmp/testinstall;
echo ........................................................................;
echo Chosen destination is $DEST;
echo All files will be compiled and installed inside directory.;
echo The sourcefiles will be copied to the destination and also kept in the intial directory but you can safely remove them.;
echo Please do not alter the directory structure or future includes will be impossible.;
echo ........................................................................;
mkdir $DEST/headers;
mkdir $DEST/objects;
cp -Rv ./headers/* $DEST/headers;
g++ -v -c ./sources/* -I $DEST/headers;
mv -v ./*.o $DEST/objects;
g++ -v -o $DEST/StudentEntryManipulationProgram $DEST/objects/*;
echo ........................................................................;
echo Thank you for installing Student Entry Manipulator by Sotiris and Nefeli.;
echo Have a nice day!!;
echo ........................................................................;

