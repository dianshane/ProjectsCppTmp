//Class Student for student entry manipulation program
#ifndef student_definition_h
#define student_definition_h
#include <string>
  class Student
    {
      //attributes
      public:
	int AM;
	std::string Name;
	unsigned int Sem;
      private:
	unsigned int Sub;
	float * Pts;
	float MO;
	Subject * subjects;
	unsigned int sub_count;
 
      //functions
      public:
	void small(int,std::string);
	void big(int,std::string,unsigned int);
	void extended(int,std::string,unsigned int,unsigned int);
	void grade(float ,float);
	void print();
	void SmallPrint();
	void SubPrint();
	void clone(Student,int,std::string);
	void copy(Student);
	void mo();
      //overloads
	Subject *operator+=(Subject);
	Student operator=(const Student);
	int operator<(Student);
	int operator<=(Student);
	int operator>(Student);
	int operator>=(Student);
	int operator==(Student);
	
      //setters and getters 
      public:
      unsigned int getSub(void)
	{
	  return Sub;
	} 
      void setSub(unsigned int Sub)      
	{
	  this->Sub=Sub;
	}
      float *getPts(void)
	{
	  return Pts;
	}
      void setPts(float *Pts)
	{
	  this->Pts=Pts;
	}
      Subject *getSubjects(void)
	{
	  return this->subjects;
	}
      void setSubjects(Subject *sub)
	{
	  this->subjects=sub;
	}
      unsigned int getSub_count(void)
	{
	  return this->sub_count;
	}
      void setSub_count(unsigned int sc)
	{
	  this->sub_count=sc;
	}
    };
#endif
