#include <subject_definition.h>
#include <stdlib.h>
#include <time.h>
#include <student_definition.h>
#include <string>
#include <functions.h>
#include <global.h>

int main(void)
  {
    srand (time(NULL));
    Global global;
    global.obj=initiallizer(global.obj,global.sub);
    start();//visual
    menu(global.obj);//everything is being controled through this function
    return 0;
  }
