#include <subject_definition.h>
#include <student_definition.h>
#include <stdio.h>
#include <stdlib.h>
#include <functions.h>
    void start()
      {
	printf("............................................................\n");
	printf("............................................................\n");
	printf("Welcome to Student Manipulation Program!\n");
	printf("For testing purposes there are two students already created.\n");
	printf("These are Mary (with A.M.=1) and John (with A.M.=2).\n");
	printf("The rest of the info for each of them is generated randomly.\n");
	printf("Feel free to try out any command on them!\n");
	printf("Have fun!!\n");
	printf("............................................................\n");
	printf("............................................................\n");
      }



  Student *initiallizer(Student *obj,Subject *sub) //generates initial 2 students so table is not empty and all commands may be used
    {
      Student *ptr=new Student [2];
      for(int j=0;j<2;j++)
	{	
	  ptr[j].AM=j+1;
	  if(j==0)
	    ptr[0].Name="Mary";
	  if(j==1)
	  {
	    ptr[1].Name="John";
	    //ptr[j].Sem=3;
	  }
	  ptr[j].Sem=(rand()%10)+1; //10 semesters max for our programm initializer
	  ptr[j].setSub(3);
	  ptr[j].setPts((float*) new float [3]);//3 subjects just for testing
	  for(int i=0;i<3;i++)
	    ptr[j].grade((float) (i+1),(float) (rand()%100));
	}
      ptr[0].mo();
      ptr[1].mo();
      
      Subject *tmp=new Subject[2];
      tmp[0]=sub[0];
      tmp[1]=sub[6];
      ptr[0].setSubjects(tmp);
      delete[] tmp;
      
      Subject *temp=new Subject[4];
      temp[0]=sub[1];
      temp[1]=sub[2];
      temp[2]=sub[3];
      temp[3]=sub[9];
      ptr[1].setSubjects(temp);
      delete[] temp;
      
    return ptr;
    }

void subject()
  {
    Subject *tmp=new Subject[10];
    tmp[0].subject_code="hst";tmp[0].name_sub="History";tmp[0].sem_sub=3;tmp[0].setCode(100);
    tmp[1].subject_code="mat";tmp[1].name_sub="Math";tmp[1].sem_sub=1;tmp[1].setCode(101);
    tmp[2].subject_code="pro";tmp[2].name_sub="Programming";tmp[2].sem_sub=1;tmp[2].setCode(102);
    tmp[3].subject_code="oop";tmp[3].name_sub="Object Oriented Programming";tmp[3].sem_sub=3;tmp[3].setCode(103);
    tmp[4].subject_code="che";tmp[4].name_sub="Chemistry";tmp[4].sem_sub=6;tmp[4].setCode(104);
    tmp[5].subject_code="alg";tmp[5].name_sub="Algebra";tmp[5].sem_sub=5;tmp[5].setCode(105);
    tmp[6].subject_code="sta";tmp[6].name_sub="Statistics";tmp[6].sem_sub=7;tmp[6].setCode(106);
    tmp[7].subject_code="db";tmp[7].name_sub="Data Bases";tmp[7].sem_sub=8;tmp[7].setCode(107);
    tmp[8].subject_code="os";tmp[8].name_sub="Operating Systems";tmp[8].sem_sub=9;tmp[8].setCode(108);
    tmp[9].subject_code="net";tmp[9].name_sub="Networks";tmp[9].sem_sub=10;tmp[9].setCode(109);
  }