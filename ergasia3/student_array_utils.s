	.file	"student_array_utils.cc"
	.section	.text._ZN7StudentC2Ev,"axG",@progbits,_ZN7StudentC5Ev,comdat
	.align 2
	.weak	_ZN7StudentC2Ev
	.type	_ZN7StudentC2Ev, @function
_ZN7StudentC2Ev:
.LFB619:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSsC1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE619:
	.size	_ZN7StudentC2Ev, .-_ZN7StudentC2Ev
	.weak	_ZN7StudentC1Ev
	.set	_ZN7StudentC1Ev,_ZN7StudentC2Ev
	.section	.text._ZN7StudentD2Ev,"axG",@progbits,_ZN7StudentD5Ev,comdat
	.align 2
	.weak	_ZN7StudentD2Ev
	.type	_ZN7StudentD2Ev, @function
_ZN7StudentD2Ev:
.LFB622:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rdi
	call	_ZNSsD1Ev
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE622:
	.size	_ZN7StudentD2Ev, .-_ZN7StudentD2Ev
	.weak	_ZN7StudentD1Ev
	.set	_ZN7StudentD1Ev,_ZN7StudentD2Ev
	.section	.text._ZN7StudentC2ERKS_,"axG",@progbits,_ZN7StudentC5ERKS_,comdat
	.align 2
	.weak	_ZN7StudentC2ERKS_
	.type	_ZN7StudentC2ERKS_, @function
_ZN7StudentC2ERKS_:
.LFB625:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rax
	movl	(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, (%rax)
	movq	-16(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	-8(%rbp), %rax
	addq	$8, %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	_ZNSsC1ERKSs
	movq	-16(%rbp), %rax
	movl	16(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, 16(%rax)
	movq	-16(%rbp), %rax
	movl	20(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, 20(%rax)
	movq	-16(%rbp), %rax
	movq	24(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 24(%rax)
	movq	-16(%rbp), %rax
	movss	32(%rax), %xmm0
	movq	-8(%rbp), %rax
	movss	%xmm0, 32(%rax)
	movq	-16(%rbp), %rax
	movq	40(%rax), %rdx
	movq	-8(%rbp), %rax
	movq	%rdx, 40(%rax)
	movq	-16(%rbp), %rax
	movl	48(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, 48(%rax)
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE625:
	.size	_ZN7StudentC2ERKS_, .-_ZN7StudentC2ERKS_
	.weak	_ZN7StudentC1ERKS_
	.set	_ZN7StudentC1ERKS_,_ZN7StudentC2ERKS_
	.text
	.globl	_Z8generateiP7Student
	.type	_Z8generateiP7Student, @function
_Z8generateiP7Student:
.LFB617:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	.cfi_lsda 0x3,.LLSDA617
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, -132(%rbp)
	movq	%rsi, -144(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -64(%rbp)
	movl	-132(%rbp), %eax
	movslq	%eax, %rbx
	movabsq	$164381386399023104, %rax
	cmpq	%rax, %rbx
	ja	.L5
	movq	%rbx, %rax
	salq	$3, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	addq	$8, %rax
	jmp	.L6
.L5:
	movq	$-1, %rax
.L6:
	movq	%rax, %rdi
.LEHB0:
	call	_Znam
.LEHE0:
	movq	%rax, %r13
	movq	%rbx, 0(%r13)
	leaq	8(%r13), %r12
	leaq	-1(%rbx), %rax
	movq	%rax, %r14
	movq	%r12, %r15
.L8:
	cmpq	$-1, %r14
	je	.L7
	movq	%r15, %rdi
.LEHB1:
	call	_ZN7StudentC1Ev
.LEHE1:
	addq	$56, %r15
	subq	$1, %r14
	jmp	.L8
.L7:
	leaq	8(%r13), %rax
	movq	%rax, -72(%rbp)
	movl	$0, -52(%rbp)
.L10:
	movl	-132(%rbp), %eax
	subl	$1, %eax
	cmpl	-52(%rbp), %eax
	jle	.L9
	movl	-52(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	-64(%rbp), %rax
	addq	%rax, %rdx
	leaq	-128(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
.LEHB2:
	call	_ZN7StudentC1ERKS_
.LEHE2:
	movl	-52(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	-72(%rbp), %rax
	addq	%rax, %rdx
	leaq	-128(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, %rdi
.LEHB3:
	call	_ZN7Student4copyES_
.LEHE3:
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
.LEHB4:
	call	_ZN7StudentD1Ev
	addl	$1, -52(%rbp)
	jmp	.L10
.L9:
	cmpq	$0, -64(%rbp)
	je	.L11
	movq	-64(%rbp), %rax
	subq	$8, %rax
	movq	(%rax), %rax
	salq	$3, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	-64(%rbp), %rax
	leaq	(%rdx,%rax), %rbx
.L13:
	cmpq	-64(%rbp), %rbx
	je	.L12
	subq	$56, %rbx
	movq	%rbx, %rdi
	call	_ZN7StudentD1Ev
.LEHE4:
	jmp	.L13
.L12:
	movq	-64(%rbp), %rax
	subq	$8, %rax
	movq	%rax, %rdi
	call	_ZdaPv
.L11:
	movq	-72(%rbp), %rax
	jmp	.L21
.L19:
	movq	%rax, %r15
	testq	%r12, %r12
	je	.L16
	movq	%r14, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	salq	$3, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	subq	$56, %rax
	leaq	(%r12,%rax), %rbx
.L17:
	cmpq	%r12, %rbx
	je	.L16
	subq	$56, %rbx
	movq	%rbx, %rdi
	call	_ZN7StudentD1Ev
	jmp	.L17
.L16:
	movq	%r15, %rbx
	movq	%r13, %rdi
	call	_ZdaPv
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB5:
	call	_Unwind_Resume
.LEHE5:
.L20:
	movq	%rax, %rbx
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN7StudentD1Ev
	movq	%rbx, %rax
	movq	%rax, %rdi
.LEHB6:
	call	_Unwind_Resume
.LEHE6:
.L21:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE617:
	.globl	__gxx_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA617:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE617-.LLSDACSB617
.LLSDACSB617:
	.uleb128 .LEHB0-.LFB617
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB617
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L19-.LFB617
	.uleb128 0
	.uleb128 .LEHB2-.LFB617
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB617
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L20-.LFB617
	.uleb128 0
	.uleb128 .LEHB4-.LFB617
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB5-.LFB617
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB6-.LFB617
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
.LLSDACSE617:
	.text
	.size	_Z8generateiP7Student, .-_Z8generateiP7Student
	.section	.rodata
	.align 8
.LC0:
	.string	"............................................................"
.LC1:
	.string	"No match found!!"
	.text
	.globl	_Z6searchiP7Studenti
	.type	_Z6searchiP7Studenti, @function
_Z6searchiP7Studenti:
.LFB627:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, -24(%rbp)
	movl	$0, -4(%rbp)
.L26:
	movl	-4(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	-32(%rbp), %rax
	addq	%rdx, %rax
	movl	(%rax), %eax
	cmpl	-20(%rbp), %eax
	je	.L23
	movl	-4(%rbp), %eax
	cmpl	-24(%rbp), %eax
	jle	.L24
	movl	$.LC0, %edi
	call	puts
	movl	$.LC1, %edi
	call	puts
	movl	$.LC0, %edi
	call	puts
	movl	$-1, %eax
	jmp	.L25
.L24:
	addl	$1, -4(%rbp)
	jmp	.L26
.L23:
	movl	-4(%rbp), %eax
.L25:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE627:
	.size	_Z6searchiP7Studenti, .-_Z6searchiP7Studenti
	.section	.rodata
.LC2:
	.string	"A.M.---Name---Semester"
	.text
	.globl	_Z9print_allP7Studenti
	.type	_Z9print_allP7Studenti, @function
_Z9print_allP7Studenti:
.LFB628:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	$.LC0, %edi
	call	puts
	movl	$.LC2, %edi
	call	puts
	movl	$.LC0, %edi
	call	puts
	movl	$0, -4(%rbp)
.L29:
	movl	-4(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jge	.L28
	movl	-4(%rbp), %eax
	cltq
	salq	$3, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	movq	-24(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, %rdi
	call	_ZN7Student10SmallPrintEv
	addl	$1, -4(%rbp)
	jmp	.L29
.L28:
	movl	$.LC0, %edi
	call	puts
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE628:
	.size	_Z9print_allP7Studenti, .-_Z9print_allP7Studenti
	.ident	"GCC: (GNU) 5.3.0"
	.section	.note.GNU-stack,"",@progbits
