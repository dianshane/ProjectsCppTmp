#include <my_string.h>
#include <stdio.h>
#include <iostream>
#include <global.h>

bool dev_registration(Global global)
	{
		std::string tmp_name;
		std::string tmp_email;

		printf("Please type in your Name:");
		std::cin>>tmp_name;

		printf("Please type in your email:");
		std::cin>>tmp_email;

		global.system_d->extend(global);
		global.dev[global.dev_size].Register(tmp_name,tmp_email);

		printf("Thank you for your registration! Your developer id is %i!\n",global.dev_size-1);
	return 1;
	}