#include <global.h>
#include <load.h>
#include <save.h>
#include <fstream>
#include <my_cstring.h>
#include <iostream>

void save::Developer(Global global)
	{
		std::fstream dev_file;
		dev_file.open(global.path+"/developer.mad",std::ios::out | std::ios::trunc);
		dev_file<<("just another test writing into file with cpp again")<<(";\n");
		dev_file.close();
		std::cout<<global.path<<std::endl;
	}

void load::Developer(Global global)
	{
		std::fstream dev_file;
		dev_file.open(global.path+"/developer.mad",std::ios::in);
			for(int j=0;j<global.dev_size;j++)
				{
					global.system_d->extend(global);
					std::string tmp_name;
					std::getline(dev_file,tmp_name);

					std::string tmp_email;
					std::getline(dev_file,tmp_email);

					char *name_char;
					std::strcpy(name_char,tmp_name.std::string::c_str());

					global.dev[j].basic(global,name_char,tmp_email);
					global.dev[j].print();
				}
		dev_file.close();
	}