#include <global.h>
#include <game.h>
#include <office.h>

void game_array_extender(Global global)
	{
		global.g_size++;
		game *tmp_games=global.games;
		global.games=new game[global.g_size];
		for(int j=0;j<global.g_size-1;j++)
			global.games[j]=tmp_games[j];
		delete tmp_games;
	}

void office_array_extender(Global global)
	{
		global.o_size++;
		office *tmp_office=global.office_apps;
		global.office_apps=new office[global.o_size];
		for(int j=0;j<global.o_size-1;j++)
			global.office_apps[j]=tmp_office[j];
		delete tmp_office;
	}
