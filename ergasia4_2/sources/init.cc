#include <global.h>
#include <my_cstring.h>
#include <developer.h>
#include <iostream>
#include <fstream>
#include <init.h>

Init::Init()
	{
	}

void Init::devel_creator(Global global)//creates an initial developer for testing purposes
	{
		global.dev=new developer[1];
		global.dev_size=1;
		global.dev->Register("Giorgos Meletiou","gmele@uniwa.gr");
	}


void Init::d_size(Global *global)
	{
		std::fstream dev_file;
		dev_file.open(global->path+"/developer.mad",std::ios::in);
			std::string line;
			std::getline(dev_file,line);
			sscanf(line.std::string::c_str(),"%i",global->dev_size);
			std::cout<<global->dev_size;
		dev_file.close();
	}