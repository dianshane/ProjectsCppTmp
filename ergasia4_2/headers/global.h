#include <game.h>
#include <office.h>
#include <developer.h>

#ifndef global_h
#define global_h

class Global
	{
		//attributes
		public:
			//game list
			game *games;
			int g_size;

			//office app list
			office *office_apps;
			int o_size;

			//developer list
			developer *dev;
			int dev_size;
			//object for system use (being used to call some memory control functions)
			developer *system_d;

			//path to data dir
			std::string path;

		//functions
		public:
			Global();
			void office_extend();
			void game_extend();
	};

#endif