#include <usr.h>
#include <protected_stdio.h>

#ifndef file_h
#define file_h

class usr::file 
    {
        // attributes
        public:
            FILE *loc;

        // funtions
        public:
            file();
            FILE *open(const char *, char);
            int close(void);
};

#endif
