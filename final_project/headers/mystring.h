#include <usr.h>
#include <stdio.h>
#include <protected_string.h>
#include <cstring>

#ifndef usr_string_h
#define usr_string_h
class usr::string 
    {
        
        //attributes
        public:
            char *String;
            int size;
        
        //funtions
        public:
            string();
            void al(void);
            void ral(void);
            void del();
            char *cstring(void);
            //~string();
        
        //overloads
        public:
            string operator=(const char *src) 
                {
                    if (strlen(src)!=0) 
                        {
                            delete[] this->String;
                            this->size = strlen(src);
                            this->String = new char[this->size+1];
                            strcpy(this->String, src);
                        }

                    else 
                        {
                            this->size = strlen(src);
                            this->String = new char[this->size+1];
                            strcpy(this->String, src);
                        }
                    return *this;
                }

            char * operator+(const char *src) 
                { 
                    char *tmp;
                    strcpy(tmp, this->String);
                    strcat(tmp, src);
                    return tmp;
                }

    };
#endif