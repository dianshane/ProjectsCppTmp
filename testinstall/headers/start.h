#ifndef start
#define start
#include <stdio.h>
void hello()
      {
	printf("............................................................\n");
	printf("............................................................\n");
	printf("Welcome to Student Manipulation Program!\n");
	printf("For testing purposes there are two students already created.\n");
	printf("These are Mary (with A.M.=1) and John (with A.M.=2).\n");
	printf("The rest of the info for each of them is generated randomly.\n");
	printf("Feel free to try out any command on them!\n");
	printf("Have fun!!\n");
	printf("............................................................\n");
	printf("............................................................\n");
      }
#endif