#ifndef init
#define init
#include <student_declaration.h>
#include <stdlib.h>
Student *initiallizer(Student *obj) //generates initial 2 students so table is not empty and all commands may be used
    {
      Student *ptr=new Student [2];
      for(int j=0;j<2;j++)
	{	
	  ptr[j].AM=j+1;
	  if(j==0)
	    ptr[0].Name="Mary";
	  if(j==1)
	  {
	    ptr[1].Name="John";
	    //ptr[j].Sem=3;
	  }
	  ptr[j].Sem=(rand()%10)+1; //10 semesters max for our programm initializer
	}
    return ptr;
    }
#endif