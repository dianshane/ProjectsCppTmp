//Class Student for student entry manipulation program
#ifndef student_functions_h
#define student_functions_h
#include <stdio.h>
#include <iostream>
#include <student_declaration.h>
  void Student::small(int AM,std::string Name)
    {
      this->AM=AM;
      this->Name=Name;
      //memcpy((char*) &(this->Name),(const char*) Name,strlen(Name));
      this->Sem=1;
      this->Sub=0;
    }

  void Student::big(int AM,std::string Name,unsigned int Sem)
    {
      this->AM=AM;
      this->Name=Name;
      //strcpy((char*) &(this->Name),(const char*) Name);
      this->Sem=Sem;
      this->Sub=0;
    }
	  

	  
  void Student::extended(int AM,std::string Name,unsigned int Sem,unsigned int Sub)
    {
      this->AM=AM;
      this->Name=Name;
      //memcpy((char*) &(this->Name),(const char*) Name,strlen(Name));
      this->Sem=Sem;
      this->Sub=Sub;
      //this->Pts=Pts;
    }


  void Student::print()
    {
      printf("............................................................\n");
      printf("A.M.---Name---Semester\n");
      printf("............................................................\n");
      std::cout <<AM<<"---"<<Name<<"---"<<Sem<<std::endl;
      //printf("%i %u \n",AM,Sem);
      //printf("%s",Name);
      printf("............................................................\n");
      printf("............................................................\n");
      printf("Subject---Grade\n");
      for(int i=0;i<2*(this->Sub);i+=2)
	{
	  printf("%i---",((i+2)/2));
	  printf("%.0f \n",(this->Pts)[i+1]);
	}
      printf("M.O.=%.1f\n",MO);
      printf("............................................................\n");
	
    
    }
  void Student::SmallPrint()
    {
      std::cout <<AM<<"---"<<Name<<"---"<<Sem<<std::endl;
    }


  void Student::copy(Student src)
    {
      //this->AM=am;
      this->AM=src.AM;
      //this->Name=name;
      this->Name=src.Name;
      this->Sem=src.Sem;
      this->Sub=src.Sub;
      this->Pts=src.Pts;
      this->MO=src.MO;
    }
    
    void Student::clone(Student src,int am,std::string name)
    {
      this->AM=am;
      //this->AM=src.AM;
      this->Name=name;
      //this->Name=src.Name;
      this->Sem=src.Sem;
      this->Sub=src.Sub;
      this->Pts=src.Pts;
      this->MO=src.MO;
    }
  void Student::grade(float s,float g)
    {
      this->Pts[2*(int)(s)-1]=g;
    }

  void Student::mo()
    {
    float tmp=0;
    int sub=(int) Sub;
    for(int i=1;i<(2*sub);i+=2)
      {
	tmp+=Pts[i];
      }
      //printf("test %f\n",tmp);
      //printf("test %i\n",sub);
    this->MO=(tmp/sub);
  }
  
#endif