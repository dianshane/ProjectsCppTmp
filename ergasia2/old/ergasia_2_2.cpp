#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
//#include <time.h>

using namespace std;
//srand (time());


	class Student

		{
		//attributes
			public:
				int AM;
				string Name;
				unsigned int Sem;
			private:
				unsigned int Sub;
				float * Pts;
				float MO;

		//functions
			public:
		 		void small(int,string);
				void big(int,string,unsigned int);
				void extended(int,string,unsigned int,unsigned int,float *);
				void grade(float ,float);
				void print(void);
				void copy(Student);
				void mo();
		//setters and getters 
			public:
				unsigned int getSub(void)
					{
						return Sub;
					} 
				void setSub(unsigned int Sub)
					{
						this->Sub=Sub;
					}
				float *getPts(void)
					{
						return Pts;
					}
				void setPts(float *Pts)
					{
						this->Pts=Pts;
					}
		};




		void Student::small(int AM,string Name)
			{
				this->AM=AM;
				this->Name=Name;
				this->Sem=1;
				this->Sub=0;
			}

		void Student::big(int AM,string Name,unsigned int Sem)
			{
				this->AM=AM;
				this->Name=Name;
				this->Sem=Sem;
				this->Sub=0;
			}
	
	
	
		void Student::extended(int AM,string Name,unsigned int Sem,unsigned int Sub,float * Pts)
			{
				this->AM=AM;
				this->Name=Name;
				this->Sem=Sem;
				this->Sub=Sub;
				this->Pts=Pts;
			}


		void Student::print()
			{
				printf("A.M. Name Semester\n");
				printf("%i %s %i",AM,Name,Sem);
				printf("...............................\n");
				printf("Subject Grade\n");
				for(int i=0;i<(this->Sub);i++)
					{
						printf("%i ",(i+1));
						printf("%f ",(this->Pts)[i+1]);
					}
				printf("M.O.=%f",MO);
			}


		void Student::copy(Student src)
			{
				this->AM=src.AM;
				this->Name=src.Name;
				this->Sem=src.Sem;
				this->Sub=src.Sub;
				this->Pts=src.Pts;
				this->MO=src.MO;
			}
		void Student::grade(float s,float g)
			{
				this->Pts[(int)(s-1)]=g;
			}
		
		void Student::mo()
			{
				float tmp;
				for(int i=0;i<Sub;i++)
					{
						tmp+=Pts[i];
					}
				this->MO=tmp/Sub;
			}
			
int main()
	{
		Student *obj
		obj=initiallizer(obj);
		
		printf("%i",obj[0].AM);
	}
			
			
Student *initiallizer(Student *obj) //generates initial 2 students so table is not empty and all commands may be used
	{
		Student *ptr=new Student [2];
		for(int j=0;j<2;j++)
			{	
				ptr[j].AM=j+1;
				if(j==0)
				ptr[0].Name="Mary";
				if(j==1)
				ptr[1].Name="John";
				ptr[j].Sem=3;
				//ptr[j].Sem=(rand()%10)+1; //10 semesters max for our programm initializer
			}
	return ptr;
	}

