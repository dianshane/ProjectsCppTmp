#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <time.h>

using namespace std;
srand (time());


	class Student

		{
		//attributes
			public:
				int AM;
				string Name;
				unsigned int Sem;
			private:
				unsigned int Sub;
				float * Pts;
				float MO;

		//functions
			public:
		 		void small(int,string);
				void big(int,string,unsigned int);
				void extended(int,string,unsigned int,unsigned int,float *);
				void grade(float ,float);
				void print(void);
				void copy(Student);
				void mo();
		//setters and getters 
			public:
				unsigned int getSub(void)
					{
						return Sub;
					} 
				void setSub(unsigned int Sub)
					{
						this->Sub=Sub;
					}
				float *getPts(void)
					{
						return Pts;
					}
				void setPts(float *Pts)
					{
						this->Pts=Pts;
					}
		};




		void Student::small(int AM,string Name)
			{
				this->AM=AM;
				this->Name=Name;
				this->Sem=1;
				this->Sub=0;
			}

		void Student::big(int AM,string Name,unsigned int Sem)
			{
				this->AM=AM;
				this->Name=Name;
				this->Sem=Sem;
				this->Sub=0;
			}
	
	
	
		void Student::extended(int AM,string Name,unsigned int Sem,unsigned int Sub,float * Pts)
			{
				this->AM=AM;
				this->Name=Name;
				this->Sem=Sem;
				this->Sub=Sub;
				this->Pts=Pts;
			}


		void Student::print()
			{
				printf("A.M. Name Semester\n");
				printf("%i %s %i",AM,Name,Sem);
				printf("...............................\n");
				printf("Subject Grade\n");
				for(int i=0;i<(this->Sub);i++)
					{
						printf("%i ",(i+1));
						printf("%f ",(this->Pts)[i+1]);
					}
				printf("M.O.=%f",MO);
			}


		void Student::copy(Student src)
			{
				this->AM=src.AM;
				this->Name=src.Name;
				this->Sem=src.Sem;
				this->Sub=src.Sub;
				this->Pts=src.Pts;
				this->MO=src.MO;
			}
		void Student::grade(float s,float g)
			{
				this->Pts[(int)(s-1)]=g;
			}
		
		void Student::mo()
			{
				float tmp;
				for(int i=0;i<Sub;i++)
					{
						tmp+=Pts[i];
					}
				this->MO=tmp/Sub;
			}

	

		Student *obj;
		Student *constructor(Student *);
		void menu(void);
		void help(void);
		void object(int ,int ,Student *);
		Student * write(int ,Student *);
		int search(int ,Student *,int );
		Student *generate(int ,Student *);
		void vathmos(int ,Student * );
		float *extend(int,float *,Student *);




int main()
	{
		Student *obj;
		obj=constructor(obj);
		menu();
	
			
	}


		
	void menu()
		{
			Student *obj; //isos kalytera global
			int size=0; //mallon 2 gia toys arxikoys
			char g;
			for(int i=0;;i++)
				{
					printf("Please insert command:");
					scanf(" %c",&g);
						if(g=='n') //new student
							{
								size++;
								object(1,size,obj);
							}
						else if(g=='c') //copy  existing student into new registry
							{
								size++;
								object(2,size,obj);
							}
						else if(g=='x') //exit
							break;
						else if(g=='m') //manual
							help();
						else if(g=='e') //edit
							object(2,size,obj);
						else if(g=='p') //print
							object(4,size,obj);
						else
							{
								printf("Invalid command!\n");
								help();
							}


				}

		}

	
	void help()
		{
			printf("............................................................\n");
			printf("Student entry manipulation programm.\n");
			printf("............................................................\n");
			printf("............................................................\n");
			printf("Manual:\n");
			printf("............................................................\n");
			printf("n-new student entry\n");
			printf("c-copy an entry to a new one (clone)\n");
			printf("e-edit grades\n");
			printf("p-print table of a Student\n");
			printf("m-display this manual\n");
			printf("x-exit\n");
			printf("............................................................\n");
		}


	void object(int operation,int size,Student *obj)
		{
			if(operation==1)
				{
					obj=write(size,obj);
				}
			if(operation==2)
				{
					int temp;
					printf("Please insert the A.M. of the student that is to be cloned:");
					scanf("%i",&temp);
					int f=search(temp,obj,size);
						if(f==-1)
							object(2,size,obj);

					obj=generate(size,obj);
					obj[size-1].copy(obj[f]);

				}
			 if(operation==3)
				{
					int temp;
					float g;
					float s;
					printf("Please insert the A.M. of the student's grades you want to change:");
					scanf("%i",&temp);
					int f=search(temp,obj,size);
						if(f==-1)
							object(2,size,obj);
					printf("Please insert the number of the subject you wanna change:");
					scanf("%f",&s);
						if(s<size)
							{
								printf("Please insert new grade for subject:");
								scanf("%f",&g);
								obj[f].grade(s,g);
								obj[f].mo();
							}
						else if(s==size+1)
							{
								float *temp=obj[f].getPts();
								float *tmp=extend(f,temp,obj);
								obj[f].setPts(tmp);
								printf("Please insert new grade for subject:");
								scanf("%f",&g);
								obj[f].grade(s,g);
								obj[f].setSub(obj[f].getSub()+1);
								obj[f].mo();

							}
						else 
							{
								printf("There is no such subject nor the option to generate new selected!\n");
								object(3,size,obj);
							}
						

				}


			 if(operation==4)
			 	{	
					int tmp;
					printf("Please insert A.M. of the student whose table you want to print:");
					scanf("%i",&tmp);
					int f=search(tmp,obj,size);
					if(f+1!=0)
						obj[f].print();
				}
		}


	Student * write(int size,Student *obj)
		{
			int am;
			string name;
			unsigned int sem;
			unsigned int sub;
			float * pts;
			unsigned int a;
			//int k;

			obj=generate(size,obj);
					
					printf("please insert 'AM':");
					scanf("%i",&am);
					printf("please insert name:");
					cin >>name; 
					printf("please insert semester (If your'e done insert '-1'):");
					scanf("%i",&a);

					if(a!=-1)
						{
							a=sem;
							printf("please insert sebjects of success (If your'e done insert '-1'):");
							scanf("%i",&a);
							if(a!=-1)
								{
									a=sub;
									vathmos((size-1),obj);
									obj[size-1].extended(am,name,sem,sub,pts);
									obj[size-1].mo();
								}
								else
									{
										//k=2;
										obj[size-1].big(am,name,sem);

									}


						}
						else 
							{
								//k=1;
								obj[size-1].small(am,name);
							}
					/*
					if(k==1)
						obj[size-1].minor(am,name);
					if(k==2)
						obj[size-1].major(am,name,sem);
					*/


		return obj;
		}	

	Student *generate(int size,Student *obj)
		{
			Student *tmp=obj;
			Student *ptr=new Student [size];
				for(int j=0;j<(size-1);j++)
					{
						ptr[j].copy(tmp[j]);
					}
				delete[] tmp;
		return ptr;
		}


	int search(int  am,Student *obj,int size)
		{
		int k=0;
			for(;obj[k].AM!=am;k++)		
				{
					if(k>size-1)
						{
								printf("............................................................\n");
							printf("No match found!!\n");
								printf("............................................................\n");
							return (-1);
						}
				}
		return k;
		}

	void vathmos(int f,Student * obj)
			{
				obj[f].setPts(new float [2*obj[f].getSub()]);
				float *tmp=obj[f].getPts();
				for(int j=0;j<obj[f].getSub();j++)
					
					{
						tmp[j]=j;	
						printf("Please insert grade for subject %i:",(j+1));
						scanf("%f",&tmp[j+1]);
					}
									
			}


	float *extend(int f,float *pts,Student *obj)
		{	
			unsigned int size=obj[f].getSub();
			float *tmp=pts;
			float *ptr=new float [(int) (size+1)];
				for(int j=0;j<(size);j++)
					{
						ptr[j]=tmp[j];
					}
				delete[] tmp;
		return ptr;
		}

	
	Student *constructor(Student * obj) //generates initial 2 students so table is not empty and all commands may be used
		{
			Student *ptr=new Student [2];
				for(int j=0;j<2;j++)
					{	
						ptr[j].AM=j+1;
						if(j==0)
							ptr[0].Name="Mary";
						if(j==1)
							ptr[1].Name="John";
						ptr[j].Sem=3;
						//ptr[j].Sem=(rand()%10)+1; //10 semesters max for our programm initializer
					}
					
		return ptr;
		}

	
