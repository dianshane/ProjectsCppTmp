#ifndef office_h
#define office_h
#include <developer.h>
class soft
	{
		//attributes
		public:
			char *code;
			std::string name;
			std::string *req_os;
			developer devel;
			std::string cat;
			std::string rate;
			float price;
		//functions	
		public:
		virtual soft basic(char *, std::string, std::string *, char *, std::string, std::string, float);

		};
#endif
