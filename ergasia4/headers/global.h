#ifndef global_h
#define global_h
#include <office.h>
#include <game.h>
class Global
	{
		//attributes
		public:
			office *office_list;
			game *game_list;
			developer *dev_list;
			int office_size;
			int game_size;
		//functions
		public:
			Global(void);
			void office_list_extend(void);
			void game_list_extend(void);
	};
#endif
