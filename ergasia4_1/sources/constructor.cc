#include <stdio.h>
#include <string>
#include <iostream>
#include <global.h>
#include <functions.h>

void constructor(Global global)
	{
		//checking what app we have to make
		char b;
		printf("Would you like to add a game or an office app? (type 'g' for game or 'o' for office app):");
		scanf("%c",&b);
		//requesting info
			//app name
		std::string app_name;
		printf("Please app name:");
		std::cin>>app_name;
		
		//category
		soft::category app_category;
		printf("Please choose a category for your %s (type the according number || To list all categories press 'l'):",b=='g'?"game":"app");
		scanf("%i",&app_category);
		
		//os compatibility
		printf("Please insert a list of the minimum required os system for the %s to work:",b=='g'?"game":"app");
		std::string *os_tmp=os();
		
		//developer
		int id;
		printf("Are you an already registered developer? If so please insert your id: (if not you may disregard this message and continue to registration):");
		scanf("%c",&id);

		int flag;
		do{
			flag==true?printf("Please insert your id:"):0;
			scanf("%c",&id);

			flag=false;
			if(!global.system_d.search(id))
				if(!dev_registration())
					flag=true;
		}while(flag==true);


		b=='g'?global.game_extend():global.office_extend();
		b=='g'?global.games[global.g_size].generate():global.office_apps[global.o_size].generate();
		
		
	}