#include <global.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <functions.h>

void dev_registration(Global global)
	{
		std::string tmp_name;
		std::string tmp_email;

		printf("Please type in your Name:");
		std::cin>>tmp_name;

		printf("Please type in your email:");
		std::cin>>tmp_email;

		global.system_d.Register(global,tmp_name,tmp_email);

		printf("Thank you for your registration! Your developer id is %i!\n",global.dev_size-1);
	}
