#ifndef string_h
#define string_h
#include <string>
#endif

#ifndef rating_h
#define rating_h
class rating
{
	enum Star { one, two, three, four, five } star;
	std::string user_name;
	std::string comments;
};
#endif