#include <developer.h>
#include <rating.h>

#ifndef soft_h
#define soft_h
class soft
{
	//attributes
public:
	char* code;
	std::string name;
	std::string* req_os;
	developer *devel;
	enum category {} cat;
	rating rate;
	float price;
	//functions	
public:
	virtual void basic(char*, std::string, std::string*, developer , category, rating, float);

};
#endif
