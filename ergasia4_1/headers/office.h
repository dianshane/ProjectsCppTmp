#include <soft.h>

#ifndef office_h
#define office_h
class office : public soft
{
//attributes
	public:
	   enum Format {} format;
	   std::string *avail_os;
//function
	public:
	   void generate(char*, std::string, std::string*, developer, category, rating,float, Format);
};
#endif