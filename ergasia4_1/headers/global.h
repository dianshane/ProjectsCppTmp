#include <game.h>
#include <office.h>

#ifndef global_h
#define global_h

class Global
	{
		//attributes
		public:
			game *games;
			office *office_apps;
			int o_size;
			developer system_d;
			int g_size;
			
			developer *dev;
			int dev_size;


		//functions
		public:
			Global();
			void office_extend();
			void game_extend();
	};

#endif