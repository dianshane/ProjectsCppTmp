////////////////////////////////////////////////////////////////////////
//////////////////GENERATES CLEAN RATINGS DB FILE/////////////////////
///////////////////////////////////////////////////////////////////////

#include <fstream>
#include <iostream>

using namespace std;

int main() 
    { 
        fstream rating;
        cout<<"Setting up rating list...";
        rating.open("./data/ratings.mad", ios::out | ios::trunc); 
            rating<<"2\n";//size
            rating<<"1\n";//game
            rating<<"0\n";//refferenced app
            rating<<"2\n";//rating
            rating<<"system@noexist.mad\n";//user identification
            rating<<"Just a comment\n";//comments
            rating<<"0\n";//game
            rating<<"0\n";//refferenced app
            rating<<"2\n";//rating
            rating<<"system@noexist.mad\n";//user identification
            rating<<"Just another comment\n";//comments
        rating.close();
        cout<<"Done!!\n";
    }
