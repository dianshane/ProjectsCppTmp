////////////////////////////////////////////////////////////////////////
//////////////////GENERATES CLEAN GAMES DB FILE////////////////////////
///////////////////////////////////////////////////////////////////////

#include <fstream>
#include <iostream>

using namespace std;

int main() 
    {
        fstream games;
        cout<<"Setting up games database...";
        games.open("./data/games.mad", ios::out | ios::trunc);
            games<<"1\n";//size
            games<<"1\n";//code
            games<<"MyGame\n";//name
            games<<"0\n";//developer
            games<<"1\n";//required os
            games<<"1\n";//category
            games<<"2\n";//mean rating
            games<<"1\n";//number of ratings
            games<<"35\n";//price
            games<<"true\n";//online
        games.close();
        cout<<"Done!!\n";
    }