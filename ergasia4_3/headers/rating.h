#include <mystring.h>

class Global;

#ifndef rating_h
#define rating_h
class rating
	{
            //attributes
            public:
                bool game;
                int refference;
		enum Star { zero,one,two,three,four,five } star;
		std::string user_name;
		std::string comments;

	    //funtions
            public:
                void basic(Global *, int ,int, int,std::string, std::string);
                void basicNoMRV(int,int, int, std::string, std::string);
                void extend(Global *);
	};
#endif