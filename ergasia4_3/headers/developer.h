#include <mystring.h>
#include <mycstring.h>

class Global;

#ifndef developer_h
#define developer_h
class developer
	{
	//attributes
		private:
			int code;
		public:
			char *Name;
			std::string email;

	//functions
		public:
                    developer(void);
                    void basic(Global *,char *,std::string);
    //		int search(Global,int);
    //		void Register(std::string,std::string);
                    void extend(Global *);
                    void print(void);
    		int getCode(void);
	};
#endif