#include <rating.h>
#include <office.h>
#include <developer.h>
#include <game.h>


#ifndef global_h
#define global_h

class Global
	{
		//attributes
		public:
			//game list
			game *games;
			int g_size;
                        int game_array;
                        
			//office app list
			office *office_apps;
			int o_size;
                        int o_array;

			//ratings list
                        rating *ratings;
                        int r_size;
                        int r_array;

			//developer list
			developer *dev;
                        int dev_array;
			int dev_size;

			//objects for system use (being used to call some memory control functions)
			developer *system_d;
                        game *system_g;
                        office *system_o;
                        rating *system_r;

			//path to data dir
			std::string path;
            //functions
            public:
			Global();
	};

#endif