#include <soft.h>
#include <developer.h>
#include <mystring.h>

#ifndef office_h
#define office_h

class office : public soft
	{
	//attributes
		public:
                enum Format 
			{
                        jpg,
			png,
                        csv,
			txt,
                        pdf,
			html
			};
                  char *format;
	    //functions
		public:

                  void generate(char *, std::string, int, developer *,char *,float, int, float);
                  void extend(Global *);
                  void print(void); 
                  void printFormat(void);
                  void formatC(char);
	};
#endif