#include <soft.h>
#include <developer.h>
#include <mystring.h>
#include <rating.h>

#ifndef game_h
#define game_h
class game : public soft
	{
	//attributes
		public:
		bool online;
                  enum category {
                    Action,
                    Simulation,
                    Adventure,
                    Strategy,
                    Card,
                    FPS,
                    BattleRoyale
                  }; 
		category cat;
		//functions
		public:
                    void extend(Global *);
		    void generate(char *,std::string,int,developer *,int ,float,int,float,bool);
                    void setCat(int);
                    void printAllCat(void); 
                    void printCat(void); 
                    void print(void); 
	};
#endif