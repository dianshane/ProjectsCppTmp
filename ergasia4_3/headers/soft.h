#include <mystring.h>
#include <rating.h>
#include <developer.h>

#ifndef soft_h
#define soft_h

class developer;

class soft
	{
	//attributes
		public:
		char *code;
		std::string name;
                enum os 
		    { 
		        Unix, 
		        Windows, 
		        Mac 
		    }; 
		os req_os;
		developer *devel;
	        float mean_rate;
                int ratings;
		float price;
		//functions	
		public:
		    virtual void basic(char *,std::string,int,developer *,float,int,float);
                    virtual void printOS(void); 
		    virtual void r(int);
	};
#endif