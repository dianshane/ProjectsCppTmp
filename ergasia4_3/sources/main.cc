#include <global.h>
#include <load.h>
#include <save.h>
#include <init.h>
#include <iostream>//temporary

int main()
	{
            Global global;
            load loadfile;
            save savefile;
            Init init;
            init.hello();
            init.d_size(&global);
            init.g_size(&global);
            init.o_size(&global);
            init.r_size(&global);

            std::cout<<"Loading developer profiles...";
            loadfile.Developer(&global);
            std::cout<<"Done!\n";

            std::cout<<"Syncing with game database...";
            loadfile.game(&global);
            std::cout<<"Done!\n";

            std::cout<<"Syncing with office app database...";
            loadfile.office(&global);
            std::cout<<"Done!\n";

            std::cout << "Syncing ratings list...";
            loadfile.ratings(&global);
            std::cout << "Done!\n";

            //std::cout << global.ratings[0].star << "enum test\n";

            //global.dev[1].print();
            //global.games[0].print();

            init.welcome();
            //menu(global);
            //global.office_apps[0].print();


            init.goodbye();
            
            std::cout << "Saving developer profiles to disk...";
            savefile.Developer(&global);
            std::cout<<"Done!\n";

            std::cout << "Updating game database...";
            savefile.game(&global);
            std::cout<<"Done!\n";

            std::cout << "Updating office app database...";
            savefile.office(&global);
            std::cout<<"Done!\n";

            std::cout << "Updating ratings list...";
            savefile.ratings(&global);
            std::cout<<"Done!\n";

            init.end();
	}