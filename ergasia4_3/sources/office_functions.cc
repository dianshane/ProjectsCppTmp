#include <office.h>
#include <global.h>
#include <mystring.h>
#include <iostream>

void office::generate(char *code, std::string name, int req_os,developer *devel, char *format, float rate, int nu, float price)
	{
                this->basic(code, name, req_os, devel, rate, nu, price);
                this->format = format;
	}


void office::extend(Global *global)
	{
                global->o_array++;
		office *tmp=global->office_apps;
		global->office_apps=new office[global->o_array];
                for (int j = 0; j < global->o_array - 1; j++) 
                    global->office_apps[j] = tmp[j];
	}


void office::print(void) 
    { 
        printf("App code: %s\n", this->code);
        std::cout<<"Name: "<<this->name<<"\n";
        printf("Developer information:\n");
        this->devel->print();
        std::cout << "Supported formats: \n"; printFormat();
        printf("Requirement: "); this->printOS();
        printf("Avg. Rating: %.1f stars\n",this->mean_rate);
        printf("Number of ratings: %i\n", this->ratings);
        printf("Price: $%.2f\n",this->price);
    }


void office::printFormat(void) 
    {
        for(int j=0;j<strlen(this->format);j++)
            this->formatC(this->format[j]);
    }

void office::formatC(char tmp) 
    {
      int n=tmp-'0';
      switch (n) 
        { 
              case 0:
                printf("jpg\n");
                break;

              case 1:
                printf("png\n");
                break;

              case 2:
                printf("csv\n");
                break;

              case 3:
                printf("txt\n");
                break;

              case 4:
                printf("pdf\n");
                break;

              case 5:
                printf("html\n");
                break;

        }
    }
