#include <mystring.h>
#include <developer.h>
#include <soft.h>
#include <game.h>
#include <global.h>
#include <iostream>

void game::generate(char *code,std::string name,int req_os,developer *devel,int cat,float rate,int nu,float price,bool online)
	{
		this->basic(code,name,req_os,devel,rate,nu,price);
		this->online=online;
                this->cat = category(cat);
	}


void game::extend(Global *global)
	{
                global->game_array++;
		game *tmp=global->games;
		global->games=new game[global->game_array];
                for (int j = 0; j < global->game_array - 1; j++) 
                    global->games[j] = tmp[j];
	}

void game::setCat(int c) 
    {
          switch (c) 
            { 
		case 1:
                    this->cat=Action;
		case 2:
                    this->cat=Simulation;
		case 3:
                    this->cat=Adventure;
		case 4:
                    this->cat=Strategy;
		case 5:
                    this->cat=Card;
		case 6:
                    this->cat=FPS;
		case 7:
                    this->cat=BattleRoyale;
	    }
    }

void game::printCat(void) 
    {
        switch (this->cat) 
            { 
                case category(0):
                    printf("Action\n");
                  break;
                case category(1):
                    printf("Simulation\n");
                  break;
                case category(2):
                    printf("Adventure\n");
                  break;
                case category(3):
                    printf("Strategy\n");
                  break;
                case category(4):
                    printf("Card\n");
                  break;
                case category(5):
                    printf("FPS\n");
                  break;
                case category(6):
                    printf("BattleRoyale\n");
                  break;
            }
    }

void game::printAllCat(void) 
    { 
	printf("code-category\n");
        printf("0-Action\n");
        printf("1-Simulation\n");
        printf("2-Adventure\n");
        printf("3-Strategy\n");
        printf("4-Card\n");
        printf("5-FPS\n");
        printf("6-BattleRoyale\n");
    }

void game::print(void) 
    { 
        printf("Game code: %s\n", this->code);
        std::cout<<"Name: "<<this->name<<"\n";
        printf("Developer information:\n");
        this->devel->print();
        std::cout << "Category: "; printCat();
        printf("Requirement: "); this->printOS();
        printf("Avg. Rating: %.1f stars\n",this->mean_rate);
        printf("Number of ratings: %i\n", this->ratings);
        printf("Price: $%.2f\n",this->price);
        printf("online game: "); printf(this->online==1?"yes\n":"no\n");
    }
