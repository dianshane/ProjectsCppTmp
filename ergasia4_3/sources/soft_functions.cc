#include <soft.h>
#include <mystring.h>
#include <developer.h>

void soft::basic(char *code,std::string name,int req_os,developer *devel,float rate,int nu,float price)
	{
		this->code=new char[strlen(code)];
		this->code=code;
		this->name=name;
		this->req_os=os(req_os);
		this->devel=new developer;
		this->devel=devel;
		this->mean_rate=rate;
                this->ratings=nu;
		this->price=price;
	}

void soft::printOS(void) 
    {
          switch (this->req_os) 
	    { 
		case os(0):
                    printf("Unix Kernel 3.4 or later\n");
                    break;
		case os(1):
                    printf("Windows 7 or later\n");
                    break;
		case os(2):
                    printf("MacOS High Sierra or later\n");
                    break;
            }
    }

void soft::r(int n) 
    { 
        float tmp=ratings*mean_rate;
	this->ratings++;
        tmp+=n;
        this->mean_rate=tmp/ratings;
    }