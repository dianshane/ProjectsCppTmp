#include <init.h>
#include <mycstring.h>
#include <fstream>
#include <iostream>
#include <global.h>

//Init::Init()
//	{
//	}

void Init::d_size(Global *global)
	{
		std::fstream dev_file;
		dev_file.open(global->path+"/developer.mad",std::ios::in);
			std::string line;
			std::getline(dev_file,line);
			sscanf(line.std::string::c_str(),"%i",&global->dev_size);
		dev_file.close();
	}


void Init::g_size(Global *global)
	{
		std::fstream game_file;
		game_file.open(global->path+"/games.mad",std::ios::in);
			std::string line;
			std::getline(game_file,line);
			sscanf(line.std::string::c_str(),"%i",&global->g_size);
		game_file.close();
	}


void Init::o_size(Global *global)
	{
		std::fstream office_file;
		office_file.open(global->path+"/office_apps.mad",std::ios::in);
			std::string line;
			std::getline(office_file,line);
			sscanf(line.std::string::c_str(),"%i",&global->o_size);
		office_file.close();
	}


void Init::r_size(Global *global)
	{
		std::fstream rating_file;
		rating_file.open(global->path+"/ratings.mad",std::ios::in);
			std::string line;
			std::getline(rating_file,line);
			sscanf(line.std::string::c_str(),"%i",&global->r_size);
		rating_file.close();
	}


void Init::hello(void) 
    { 
        printf("..........................................................\n");
	printf("MAD ROBOT application manager! Please wait...\n");
        printf("..........................................................\n");
    }

void Init::welcome(void) 
    { 
        printf("..........................................................\n");
        printf("..........................................................\n");
	printf("Welcome!\n");
        printf("..........................................................\n");
    }


void Init::goodbye(void) 
    { 
        printf("..........................................................\n");
        printf("..........................................................\n");
	printf("Thank you for using mad robot! Please wait one moment...\n");
        printf("..........................................................\n");
    }

void Init::end(void) 
    {
        printf("..........................................................\n");
        printf("Have a nice day!!!\n");
        printf("..........................................................\n");
    }
