#include <load.h>
#include <fstream>
#include <mycstring.h>
#include <global.h>
#include <iostream>
#include <save.h>

void load::Developer(Global *global)
	{
		std::fstream dev_file;
		dev_file.open(global->path+"/developer.mad",std::ios::in);

                //dumping first line
                std::string linedump;
                std::getline(dev_file,linedump);

                    for(int i=0;i<global->dev_size;i++)
                        {
                            global->system_d->extend(global);

                            std::string tmp_name;
                            std::getline(dev_file,tmp_name);

                            std::string tmp_email;
                            std::getline(dev_file,tmp_email);

                            char *name_char=new char[tmp_name.std::string::length()];
                            std::strcpy(name_char,tmp_name.std::string::c_str());

                            global->dev[i].basic(global,name_char,tmp_email);
                        }
		dev_file.close();
	}

void save::Developer(Global *global) 
    { 
        std::fstream dev_file;       
        dev_file.open(global->path + "/developer.mad",std::ios::out | std::ios::trunc);
        dev_file << global->dev_array << std::endl;
        for (int j = 0; j < global->dev_array; j++) 
            {
                dev_file << global->dev[j].Name<< std::endl;
                dev_file << global->dev[j].email<< std::endl;
            }
        dev_file.close();
    }


void load::game(Global *global)
	{
		std::fstream game_file;
		game_file.open(global->path+"/games.mad",std::ios::in);

                //dumping first line
                std::string linedump;
                std::getline(game_file,linedump);

                    for (int i = 0; i < global->g_size; i++) {

                  global->system_g->extend(global);

                  //code
                  std::string tmp_code;
                  std::getline(game_file, tmp_code);

                  //name
                  std::string tmp_name;
                  std::getline(game_file, tmp_name);

                  //dev
                  std::string tmp_devel;
                  int devel;
                  std::getline(game_file, tmp_devel);
                  sscanf(tmp_devel.std::string::c_str(), "%i", &devel);

                  //os
                  std::string tmp_os;
                  int os;
                  std::getline(game_file, tmp_os);
                  sscanf(tmp_os.std::string::c_str(), "%i", &os);

                  //cat
                  std::string tmp_cat;
                  int cat;
                  std::getline(game_file, tmp_cat);
                  sscanf(tmp_cat.std::string::c_str(), "%i", &cat);

                  //rating
                  std::string tmp_rating;
                  float rating;
                  std::getline(game_file, tmp_rating);
                  sscanf(tmp_rating.std::string::c_str(), "%f", &rating);

                  //rating count
                  std::string tmp_nurating;
                  int nurating;
                  std::getline(game_file, tmp_nurating);
                  sscanf(tmp_nurating.std::string::c_str(), "%i", &nurating);

                  //price
                  std::string tmp_price;
                  float price;
                  std::getline(game_file, tmp_price);
                  sscanf(tmp_price.std::string::c_str(), "%f", &price);

                  //online
                  std::string tmp_online;
                  bool online;
                  std::getline(game_file, tmp_online);
                  tmp_online == "true" ? online = 1 : online = 0;

                  char *code_char = new char[tmp_code.std::string::length()];
                  std::strcpy(code_char, tmp_code.std::string::c_str());

                  // calling contructor
                    global->games[i].generate(code_char,tmp_name,os,&(global->dev[devel]),cat,rating,nurating,price,online);
                        }
		game_file.close();
	}

void save::game(Global *global) 
    {
        std::fstream game_file;       
        game_file.open(global->path + "/games.mad",std::ios::out | std::ios::trunc);
        game_file << global->game_array << std::endl;//size

        for (int j=0;j<global->game_array;j++) 
            {
                game_file<<global->games[j].code<<"\n";//code
                game_file << global->games[j].name<<"\n";//name
                game_file<<global->games[j].devel->getCode()-1<<"\n";//dev
                game_file<<global->games[j].req_os<<"\n";//req_os
                game_file<<global->games[j].cat<<"\n";//cat
                game_file<<global->games[j].mean_rate<<"\n";//rating
                game_file<<global->games[j].ratings<<"\n";//rating count
                game_file<<global->games[j].price<<"\n";//price
                game_file<<(global->games[j].online?"true":"false")<<"\n";//online
            }
        game_file.close();
    }



void load::office(Global *global)
	{
		std::fstream office_file;
		office_file.open(global->path+"/office_apps.mad",std::ios::in);

                //dumping first line
                std::string linedump;
                std::getline(office_file,linedump);

                    for (int i = 0; i < global->o_size; i++) {

                  global->system_o->extend(global);

                  //code
                  std::string tmp_code;
                  std::getline(office_file, tmp_code);


                  //name
                  std::string tmp_name;
                  std::getline(office_file, tmp_name);

                  //dev
                  std::string tmp_devel;
                  int devel;
                  std::getline(office_file, tmp_devel);
                  sscanf(tmp_devel.std::string::c_str(), "%i", &devel);

                  //os
                  std::string tmp_os;
                  int os;
                  std::getline(office_file, tmp_os);
                  sscanf(tmp_os.std::string::c_str(), "%i", &os);

                  //cat
                  std::string tmp_format;
                  std::getline(office_file, tmp_format);
                  char *tmp_char=new char[tmp_format.std::string::length()];   
                  std::strcpy(tmp_char, tmp_format.std::string::c_str());


                  //rating
                  std::string tmp_rating;
                  float rating;
                  std::getline(office_file, tmp_rating);
                  sscanf(tmp_rating.std::string::c_str(), "%f", &rating);

                  //number of ratings
                  std::string tmp_nurating;
                  int nurating;
                  std::getline(office_file, tmp_nurating);
                  sscanf(tmp_nurating.std::string::c_str(), "%i", &nurating);

                  //price
                  std::string tmp_price;
                  float price;
                  std::getline(office_file, tmp_price);
                  sscanf(tmp_price.std::string::c_str(), "%f", &price);

                  char *code_char = new char[tmp_code.std::string::length()];
                  std::strcpy(code_char, tmp_code.std::string::c_str());

                  // calling contructor
                     global->office_apps[i].generate(code_char,tmp_name,os,&(global->dev[devel]),tmp_char,rating,nurating,price);
                        }
		office_file.close();
	}


void save::office(Global *global) 
    {
        std::fstream office_file;       
        office_file.open(global->path + "/games.mad",std::ios::out | std::ios::trunc);
        office_file << global->o_array << std::endl;//size

        for (int j=0;j<global->o_array;j++) 
            {
                office_file<<global->office_apps[j].code<<"\n";//code
                office_file << global->office_apps[j].name<<"\n";//name
                office_file<<global->office_apps[j].devel->getCode()-1<<"\n";//dev
                office_file<<global->office_apps[j].req_os<<"\n";//req_os
                std::string frmt(global->office_apps[j].format);
                office_file<<frmt<<"\n";//format
                office_file<<global->office_apps[j].mean_rate<<"\n";//rating
                office_file<<global->office_apps[j].ratings<<"\n";//rating count
                office_file<<global->office_apps[j].price<<"\n";//price
            }
        office_file.close();
    }



void load::ratings(Global *global)
	{
		std::fstream rating_file;
		rating_file.open(global->path+"/ratings.mad",std::ios::in);
                printf("sizetest %i", global->r_size);

                //dumping first line
                std::string linedump;
                std::getline(rating_file,linedump);

                    for(int i=0;i<global->r_size;i++)
                        {

                        global->system_r->extend(global);
                                
                        printf("i test %i", i);
                              //game
                              std::string tmp_game;
                              int lgame;
                              std::getline(rating_file, tmp_game);
                              sscanf(tmp_game.std::string::c_str(), "%i", &lgame);
                              printf("game test %i", lgame);
                              
                              //refference
                              std::string tmp_reff;
                              int lreff;
                              std::getline(rating_file, tmp_reff);
                              sscanf(tmp_reff.std::string::c_str(), "%i", &lreff);
                              printf("reff test %i", lreff);

                              //rating
                              std::string tmp_rating;
                              int lrating;
                              std::getline(rating_file, tmp_rating);
                              sscanf(tmp_rating.std::string::c_str(), "%i", &lrating);
                              printf("rating test %i", lrating);

                              //id
                              std::string tmp_name;
                              std::getline(rating_file, tmp_name);
                              std::cout << "nametest"<< tmp_name << std::endl;

                              //comments
                              std::string tmp_com;
                              std::getline(rating_file, tmp_com);
                              std::cout << tmp_com << "comm test\n";

                              //calling constuctor
                              global->ratings[i].basicNoMRV(lgame, lreff,lrating, tmp_name,tmp_com);
                        }
		rating_file.close();
	}

void save::ratings(Global *global) 
    {
        std::fstream rating_file;       
        rating_file.open(global->path + "/ratings.mad",std::ios::out | std::ios::trunc);
        rating_file << global->r_array << std::endl;//size

        for (int j=0;j<global->r_array;j++) 
            {
                rating_file<<global->ratings[j].game<<"\n";
                rating_file<<global->ratings[j].refference<<"\n";
                rating_file<<global->ratings[j].star<<"\n";
                rating_file<<global->ratings[j].user_name<<"\n";
                rating_file<<global->ratings[j].comments<<"\n";
            }
        rating_file.close();
        
    }
