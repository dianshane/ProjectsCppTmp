# ProjectCppTmp
Tmp directory for projects before upload in ProjectCpp.
For more info about ProjectCpp and/or the purpose of this repo visit https://github.com/dianshane/ProjectsCpp.git !

PLEASE FOLLOW THE CODE OF CONDUCT BELOW!

1)NEVER commit or in any way alter an existing file. Make your own copy and work from there.(Use something along the lines of TaskName-YourName.cc so other users know what it is).

2)ALWAYS give credit to creator if you use part of his code. (Even a comment next to his function stating his username is enough! p.e. //this function was taken from "username")

3)HAVE FUN CODING!!!


My kindest regards!
Shane
