////////////////////////////////////////////////////////////////////////
//////////////////GENERATES CLEAN DEVELOPER DB FILE/////////////////////
///////////////////////////////////////////////////////////////////////

#include <fstream>
#include <iostream>

using namespace std;

int main() 
    { 
        fstream dev;
        cout<<"Setting up developer list...";
        dev.open("./data/developer.mad", ios::out | ios::trunc); 
            dev<<"5\n";
            dev<<"SystemReserved\n";
            dev<<"system@noexist.mad\n";
            dev<<"Giorgos Meletiou\n";
            dev<<"gmele@uniwa.gr\n";
            dev << "Nefeli Rari\n";
            dev << "nefosaki@gmail.com\n";
            dev << "Jenny Mama\n";
            dev << "jenny@mama.com\n";
            dev << "Apostolos\n";
            dev << "anagnostopoulos@uniwa.gr\n";
        dev.close();
        cout<<"Done!!\n";
    }