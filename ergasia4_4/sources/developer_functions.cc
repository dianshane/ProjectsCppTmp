#include <developer.h>
#include <iostream>
#include <mystring.h>
#include <global.h>

developer::developer()
	{
	    this->Name=new char;
	}

void developer::basic(Global *global,char *Name,std::string email)
	{
		this->code=global->dev_array;
		this->Name=new char[strlen(Name)];
		std::strncpy(this->Name,Name,strlen(Name));
		this->email=email;
	}

void developer::print()
	{
		std::cout<<"Name: "<<this->Name<<std::endl;
		std::cout<<"email: "<<this->email<<std::endl;
	}

void developer::extend(Global *global)
	{
		global->dev_array++;
		developer *tmp=global->dev;
		global->dev=new developer[global->dev_array];
                for (int j = 0; j < global->dev_array - 1; j++) 
                    global->dev[j] = tmp[j];
	}

int developer::getCode(void) 
    { 
	return this->code;
    }
