#include <init.h>
#include <mycstring.h>
#include <fstream>
#include <iostream>
#include <global.h>

//Init::Init()
//	{
//	}

void Init::d_size(Global *global)
	{
		std::fstream dev_file;
		dev_file.open(global->path+"/developer.mad",std::ios::in);
			std::string line;
			std::getline(dev_file,line);
			sscanf(line.std::string::c_str(),"%i",&global->dev_size);
		dev_file.close();
	}

void Init::hello(void) 
    { 
        printf("..........................................................\n");
	printf("MAD ROBOT application manager!\n");
        printf("..........................................................\n");
    }

void Init::g_size(Global *global)
	{
		std::fstream game_file;
		game_file.open(global->path+"/games.mad",std::ios::in);
			std::string line;
			std::getline(game_file,line);
			sscanf(line.std::string::c_str(),"%i",&global->g_size);
		game_file.close();
	}

void Init::welcome(void) 
    { 
        printf("..........................................................\n");
        printf("..........................................................\n");
	printf("Welcome!\n");
        printf("..........................................................\n");
    }
