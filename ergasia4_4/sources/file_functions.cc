#include <load.h>
#include <fstream>
#include <mycstring.h>
#include <global.h>
#include <iostream>
#include <save.h>

void load::Developer(Global *global)
	{
		std::fstream dev_file;
		dev_file.open(global->path+"/developer.mad",std::ios::in);

                //dumping first line
                std::string linedump;
                std::getline(dev_file,linedump);

                    for(int i=0;i<global->dev_size;i++)
                        {
                            global->system_d->extend(global);

                            std::string tmp_name;
                            std::getline(dev_file,tmp_name);

                            std::string tmp_email;
                            std::getline(dev_file,tmp_email);

                            char *name_char=new char[tmp_name.std::string::length()];
                            std::strcpy(name_char,tmp_name.std::string::c_str());

                            global->dev[i].basic(global,name_char,tmp_email);
                        }
		dev_file.close();
	}

void save::Developer(Global *global) 
    { 
        std::fstream dev_file;       
        dev_file.open(global->path + "/developer.mad",std::ios::out | std::ios::trunc);
        dev_file << global->dev_size << std::endl;
        for (int j = 0; j < global->dev_size; j++) 
            {
                dev_file << global->dev[j].Name<< std::endl;
                dev_file << global->dev[j].email<< std::endl;
            }
        dev_file.close();
    }




void load::game(Global *global)
	{
		std::fstream game_file;
		game_file.open(global->path+"/games.mad",std::ios::in);

                //dumping first line
                std::string linedump;
                std::getline(game_file,linedump);

                    for (int i = 0; i < global->g_size; i++) {

                  global->system_d->extend(global);

                  //code
                  std::string tmp_code;
                  std::getline(game_file, tmp_code);

                  //name
                  std::string tmp_name;
                  std::getline(game_file, tmp_name);

                  //dev
                  std::string tmp_devel;
                  int devel;
                  std::getline(game_file, tmp_devel);
                  sscanf(tmp_devel.std::string::c_str(), "%i", &devel);

                  //os
                  std::string tmp_os;
                  int os;
                  std::getline(game_file, tmp_os);
                  sscanf(tmp_os.std::string::c_str(), "%i", &os);

                  //cat
                  std::string tmp_cat;
                  int cat;
                  std::getline(game_file, tmp_cat);
                  sscanf(tmp_cat.std::string::c_str(), "%i", &cat);

                  //rating
                  std::string tmp_rating;
                  float rating;
                  std::getline(game_file, tmp_rating);
                  sscanf(tmp_rating.std::string::c_str(), "%f", &rating);

                  //price
                  std::string tmp_price;
                  float price;
                  std::getline(game_file, tmp_price);
                  sscanf(tmp_price.std::string::c_str(), "%f", &price);

                  //online
                  std::string tmp_online;
                  bool online;
                  std::getline(game_file, tmp_online);
                  tmp_online == "true" ? online = 1 : online = 0;

                  char *code_char = new char[tmp_code.std::string::length()];
                  std::strcpy(code_char, tmp_code.std::string::c_str());

                  // calling contructor
                    global->games[i].generate(code_char,tmp_name,os,&(global->dev[devel]),cat,rating,price,online);
                        }
		game_file.close();
	}

void save::game(Global *global) 
    {
        std::fstream game_file;       
        game_file.open(global->path + "/games.mad",std::ios::out | std::ios::trunc);
        game_file << global->g_size << std::endl;//size

        for (int j=0;j<global->g_size;j++) 
            {
                game_file<<global->games[j].code<<"\n";//code
                game_file << global->games[j].name<<"\n";//name
                game_file<<global->games[j].devel->getCode()-1<<"\n";//dev
                game_file<<global->games[j].req_os<<"\n";//req_os
                game_file<<global->games[j].cat<<"\n";//cat
                game_file<<global->games[j].mean_rate<<"\n";//rating
                game_file<<global->games[j].price<<"\n";//price
                game_file<<(global->games[j].online?"true":"false")<<"\n";//online
            }
        game_file.close();
    }
