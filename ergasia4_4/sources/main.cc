#include <global.h>
#include <load.h>
#include <save.h>
#include <init.h>
#include <iostream>//temporary

int main()
	{
            Global global;
            load loadfile;
            save savefile;
            Init init;
            init.hello();
            init.d_size(&global);
            init.g_size(&global);

            std::cout<<"Loading developer profiles...\n";
            loadfile.Developer(&global);

            std::cout<<"Syncing with game database...\n";
            loadfile.game(&global);

            init.welcome();

            global.dev[4].print();

            global.games[0].print();

            //menu(global);

            std::cout << "Saving developer profiles to disk...\n";
            savefile.Developer(&global);
            std::cout << "Updating game database...\n";
            savefile.game(&global);
	}